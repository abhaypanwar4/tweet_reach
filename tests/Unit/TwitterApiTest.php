<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\TweetData;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TwitterApiTest extends TestCase
{

    public function testUrlValidity()
    {
        $response = $this->json('POST', '/tweet/reach', ['twitter_url' => 'https://api.twitter.com/1.1/statuses/retweets/914592637591670786']);
               $response->assertJsonFragment(['success'=>true]);
    }


    public function testUrlInValidity()
    {
        $response = $this->json('POST', '/tweet/reach', ['twitter_url' => 'https://twitter.com/BiaSciLab/status/9145927777591']);
               $response->assertJsonFragment(['success'=>false]);
    }

    public function testResponseFromDatabase()
    {
        $tweetData = TweetData::first();
        if (count($tweetData)>0) {
            $tweetId = $tweetData->tweet_id;
        } else {
            $tweetData = TweetData::create(['tweet_id'=>'914592637591670786', 'retweets'=>'0', 'tweet_reach' => '0']);
        }
        $response = $this->json('POST', '/tweet/reach', ['twitter_url' => 'https://twitter.com/BiaSciLab/status/'.$tweetData->tweet_id]);
        $response->assertJsonFragment(['source'=>'DB']);
        TweetData::where('id', $tweetData->id)->delete();
    }

    public function testResponseFromApi()
    {
        
        $response = $this->json('POST', '/tweet/reach', ['twitter_url' => 'https://twitter.com/BiaSciLab/status/914592637591670786']);
        $response->assertJsonFragment(['source'=>'API']);
    }
}
