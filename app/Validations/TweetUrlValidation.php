<?php

namespace App\Validations;

use Validator;
use Illuminate\Validation\Rule;

class TweetUrlValidation
{
    public function validateUrl(array $data)
    {
        return Validator::make($data, [
        'twitter_url'=>'required|url'
        ]);
    }
}
