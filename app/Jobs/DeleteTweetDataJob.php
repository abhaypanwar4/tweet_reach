<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\TweetData;
class DeleteTweetDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $tweetData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TweetData $tweetData)
    {
        $this->tweetData=$tweetData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(TweetData $tweetData)
    {
        TweetData::where('id', $this->tweetData->id)->delete();
    }
}
