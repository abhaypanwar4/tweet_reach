<?php
namespace App\Services;

class OAuthConnectionService
{
    
    private $requestMethod;
    private $oauth_access_token;
    private $oauth_access_token_secret;
    private $consumer_key;
    private $consumer_secret;
    private $base_url;
    public function __construct()
    {
        $this->requestMethod="GET";
        $this->oauth_access_token=env('oauth_access_token', '70178756-lqBeGOQTijHDi3ugVynWswkMPLAfN5ctjER5Ua4s6');
        $this->oauth_access_token_secret=env('oauth_access_token_secret', 'KyFstAUCHCUr72BuHfc20L0Y4hrVwiTMSl6HwFP1nqfUE');
        $this->consumer_secret=env('consumer_secret', '2y8sGyjp712Ycy0hq5vDKDc19VMA7yoOl3vi1XJYCHPh3bn3W0');
        $this->consumer_key=env('consumer_key', 'vlZNaZNilf7ibabapMVZEY2wx');
        $this->base_url = 'https://api.twitter.com/1.1/';
    }

    public function makeRequest($requestUrl)
    {
        $fullUrl=$this->base_url.$requestUrl;
        $url_parts = parse_url($requestUrl);
        $parameters=array(); //for parameters; if sent in url
        if (isset($url_parts['query'])) {
            parse_str($url_parts['query'], $parameters);
        }
        $requestUrl=$this->base_url.$url_parts['path'];
        $baseInfoString=$this->createInfoString($requestUrl, $parameters);
        $compositeKey=$this->createCompositeKey();
        $headers=$this->ReturnOAuthHeaders();
        $headers['oauth_signature']=$this->createSignature($baseInfoString, $compositeKey);
        $header=[$this->buildAuthorizationHeader($headers), 'Expect:'];
        $retweets = $this->getApiResponse($header, $fullUrl);
        return $retweets;
    }

    public function ReturnOAuthHeaders()
    {
        return $oauth = [
            'oauth_consumer_key' => $this->consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $this->oauth_access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        ];
    }

    public function buildAuthorizationHeader($headers)
    {
        $header='Authorization: OAuth ';
        $returnHeader=array();
        foreach ($headers as $key => $value) {
            $returnHeader[]= "$key=\"" . rawurlencode($value) . "\"";
        }
        return $header.=implode(", ", $returnHeader);
    }
    public function createInfoString($url, $urlArguments)
    {
        $oauthHeaders=array_merge($this->ReturnOAuthHeaders(), $urlArguments);
        $baseString=array();
        ksort($oauthHeaders);
        foreach ($oauthHeaders as $headerKey=>$headerValue) {
            $baseString[] =  "$headerKey=" . rawurlencode($headerValue);
        }
        return $this->requestMethod."&".rawurlencode($url)."&".rawurlencode(implode('&', $baseString));
    }

    public function createCompositeKey()
    {
        return rawurlencode($this->consumer_secret).'&'.rawurlencode($this->oauth_access_token_secret);
    }

    public function createSignature($baseInfo, $compositeKey)
    {
        return base64_encode(hash_hmac('sha1', $baseInfo, $compositeKey, true));
    }

    public function getApiResponse($header, $fullUrl)
    {
        $options = [
            CURLOPT_HTTPHEADER => $header,
            //CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $fullUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $result = curl_exec($feed);
        $info = curl_getinfo($feed);
        curl_close($feed);
        $responseCode = $info['http_code'];
        $retweets = array();
        $retweets['responseCode'] = $responseCode;
        $retweets['result'] = $result;
        return $retweets;
    }
}
