<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Validations\TweetUrlValidation;
use App\Services\OAuthConnectionService;
use App\Repositories\TweetDataRepository;


class TweetReachController extends Controller
{
    const API_URL = "statuses/retweets/";
    private $requestUrl;
    private $tweetId;
    private $retweetCount;
    private $tweetReach;
    private $tweetDataRepositoryObject;
    public function __construct()
    {
        $this->tweetDataRepositoryObject=new TweetDataRepository();
    }
    
    public function tweetReachView()
    {
        return view('twitter.findTweetReach');
    }
    /**
     * @param  Request
     * @return [json]
     */
    public function tweetReach(Request $request)
    {
        $data = $request->all();
        $urlResponse = $this->makeRequestURL($data);
        if ($urlResponse==1) {
            $response=$this->checkExistingTweets();
        } else {
            $response=$urlResponse;
        }
        return json_encode($response);
    }
/**
 * @return [array] 
 * returns a response array
 * and checks if the tweet 
 * info is from database or from API 
 */
    public function checkExistingTweets()
    {
        $tweet=$this->tweetDataRepositoryObject->getTweets($this->tweetId);
        if (count($tweet)==0) {
            $response=$this->fetchFromTwitter();
            if ($response['success']==true) {
                $this->persistTweet();
                $response['message']="This Tweet Is retweeted ".$this->retweetCount." times and has a reach of ".$this->tweetReach." accounts. (From Api)";
                $response['source'] = 'API';
                $response['success']=true;
                return $response;
            } else {
                return $response;
            }
        } else {
            $this->tweetReach=$tweet->tweet_reach;
            $this->retweetCount=$tweet->retweets;
            $response['message']="This Tweet Is retweeted ".$this->retweetCount." times and has a reach of ".$this->tweetReach." accounts. (From Database)";
            $response['source'] = 'DB';
            $response['success']=true;
            return $response;
        }
    }

    public function fetchFromTwitter()
    {
        $oAuthConnectionService=new OAuthConnectionService();
        $retweets = $oAuthConnectionService->makeRequest($this->requestUrl);
        if ($this->checkResponseCode($retweets['responseCode'])==-1) {
            $message['message'] = $this->getResponseMessage($retweets['result']);
            $message['success']=false;
        } else {
            $message['message']=$this->calculateTweetReach($retweets['result']);
            $message['success']=true;
        }
        return $message;
    }

    public function persistTweet()
    {
        $data['tweet_id']=$this->tweetId;
        $data['tweet_reach']=$this->tweetReach;
        $data['retweets']=$this->retweetCount;
        $this->tweetDataRepositoryObject->persistTweet($data);
    }
    public function checkResponseCode($responseCode)
    {
        if ($responseCode==200) {
            return 1;
        } else {
            return -1;
        }
    }

    /**
     * @param  [array]
     * @return [array || 1]
     * validates the input url, and gets the tweet id
     */
    public function makeRequestURL($url)
    {
        $validate = new TweetUrlValidation();
        $isValidUrl = $validate->validateUrl($url);
        if ($isValidUrl->fails()) {
            $response['message'] = $isValidUrl->errors()->all();
            $response['success'] = false;
            return $response;
        } else {
            $tweet_url = $url['twitter_url'];
            $this->requestUrl= self::API_URL.basename($tweet_url).'.json';
            $this->tweetId=basename($tweet_url);
            return 1;
        }
    }
    /**
     * @param  [json]
     * counts the tweet reach
     */
    public function calculateTweetReach($retweets)
    {
        $retweets=json_decode($retweets, true);
        $count=0;
        foreach ($retweets as $key => $value) {
            $count +=$value['user']['followers_count'];
        }
        
        $this->tweetReach=$count;
        $this->retweetCount=$retweets[0]['retweet_count'];
    }

/**
 * @param  [json]
 * @return [array]
 * returns the responses form the twitter api 
 */
    public function getResponseMessage($result)
    {
        $message = json_decode($result, true);
        $messages = array();
        foreach ($message as $key => $value) {
            $messages =  $value[0]['message'];
        }
        return $messages;
    }
}
