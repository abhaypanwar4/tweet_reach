<?php
namespace App\Repositories;

use App\TweetData;
use Carbon\Carbon;
use App\Jobs\DeleteTweetDataJob;

class TweetDataRepository
{
    public function getTweets($tweetId)
    {
        return TweetData::where('tweet_id', $tweetId)->first();
    }

    public function persistTweet($data)
    {
        $tweet=TweetData::create(['tweet_id'=>$data['tweet_id'],'retweets'=>$data['retweets'],'tweet_reach'=>$data['tweet_reach']]);
        DeleteTweetDataJob::dispatch($tweet)->delay(Carbon::now()->addMinutes(120));
    }
}
