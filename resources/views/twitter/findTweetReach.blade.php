@extends('master.template')
@section('title')
Find your tweet Reach!
@endsection

@section('content')


<div class="container">
    <section>
        <div class="row">
            <header>Enter your tweet link and find your reach!</header>
        </div>
        <form method="POST" name="findReach" class="findReach" action="{{route('submitLink')}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <input type="text" name="twitter_url" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                    <button type="submit" class="btn btn-info">GO!</button>
                </div>
            </div>
        </form>
    </section>
    <section>
        <div class='row'>
            <div class='table-responsive'>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th id='process'></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="message"></td>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="{{ URL::asset('./resources/assets/ajax_form/jquery_form.min.js') }}"></script>
<script type="text/javascript">
    $('.findReach').ajaxForm({
        // dataType: json,
        beforeSend: function () {
            $('#process').text('Processing Results...');
        },
        success: function (response) {
           var responseObject = jQuery.parseJSON(response);
            if (responseObject.success === false) {
                $('#message').text(responseObject.message);
            } else {
                $('#message').text(responseObject.message);
        }
    },
    complete: function () {
        $('#process').text('Following Results found:');
    }

});
</script>
@endsection
