<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Find Your Tweet Reach!')</title>
    @include('master.header')
</head>
<body>
    @yield('content')
    @yield('scripts')
</body>
</html>
