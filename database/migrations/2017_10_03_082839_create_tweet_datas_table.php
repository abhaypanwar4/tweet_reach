<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweet_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tweet_id')->comment('tweet id from tweer url');
            $table->bigInteger('retweets')->comment('number of retweets');
            $table->bigInteger('tweet_reach')->comment('count of a tweet reached');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweet_datas');
    }
}
