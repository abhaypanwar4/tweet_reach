# README #


### What is this repository for? ###

This is a simple Laravel application that uses Twitter's API to fetch the details of a tweet.
It calculates how many retweets a tweet has and what is its reach. 
Reach is calculated by summing up the followers of the users who have retweeted. 
Also the tweet details are stored in a database for two hours; which are deleted via an event
that fires after two hours from the time when the tweet was persisted.
The details will be shown from the database for two hours after the first search.

Version 1.0

# How do I get set up? #
 
Configuration - 
### Make changes to the project's .env file and add the following: ###

oauth_access_token='OAUTH_ACCESS_TOKEN'
oauth_access_token_secret='oauth_access_token_secret'
consumer_secret='consumer_secret'
consumer_key='consumer_key'

### Further add database and queue setting in .env file ###
 
DB_DATABASE
DB_USERNAME=
DB_PASSWORD=
Set QUEUE_DRIVER=database


## Install Dependencies ##
run: composer-update

## Setup Database ##
run php artisan migrate

# Running Tests
To Run Tests 
run phpunit command in project folder

# Running the application with event firing #
Though the application will run just fine but event firing requires the 
queue to be running. It can be done as follows:

php artisan queue:work

Just copy a tweet url and paste it in the text box. hit GO!

# Contact
Repository Owner
Abhay Panwar (abhay.panwar4@gmail.com)